use std::sync::{Arc, Mutex};
use std::ops::*;

use euclid::Vector2D;

use organs::organ::Organ;
use interaction::thread::InteractionAlgorithm;

pub struct BruteForceInteraction {
    organs: Vec<Organ>,
}

impl BruteForceInteraction {
    pub fn new() -> Self {
        BruteForceInteraction {
            organs: vec!()
        }
    }
}

impl InteractionAlgorithm for BruteForceInteraction {
    fn init(&mut self, organs_mutex: &Arc<Mutex<Vec<Organ>>>) {

        let organs = organs_mutex.lock().unwrap();
        println!("BruteForce thread: Init copying {:?} organs", organs.len());

        for organ in organs.iter() {
            self.organs.push(organ.clone());
        }
    }

    fn update(&mut self, organs_mutex: &Arc<Mutex<Vec<Organ>>>) {
        _brute_force_update(&mut self.organs);

        if let Ok(mut display_organs) = organs_mutex.lock() {
            for (organ, moved) in display_organs.iter_mut().zip(self.organs.iter()) {
                organ.set_position(*moved.position());
            }
        }
    }
}

fn _brute_force_update(organs: &mut Vec<Organ>) {
    let organs_num = organs.len();
    let mut forces = Vec::with_capacity(organs_num);
    for i in 0..organs_num {
        let organ = &organs[i];
        let mut applied_force = Vector2D::<f32>::zero();
        let interactings = organs.iter().filter(|o| organ.distance(o) < 3.5);
        for interacting in interactings {
            applied_force.add_assign(
                organ.interaction_vector(interacting)
            );
        }
        forces.push(applied_force);
    }
    for (i, organ) in organs.iter_mut().enumerate() {
        organ.move_with(forces[i].mul(0.02));
    }
}
