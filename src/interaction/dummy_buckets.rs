use std::sync::{Arc, Mutex};
use std::ops::*;

use euclid::{Point2D, point2, Vector2D};

use organs::organ::Organ;

use interaction::thread::InteractionAlgorithm;

pub struct DummyBucketsInteraction {
    organs: Vec<Organ>,
}

impl DummyBucketsInteraction {
    pub fn new() -> Self {
        DummyBucketsInteraction {
            organs: vec!()
        }
    }
}

impl InteractionAlgorithm for DummyBucketsInteraction {
    fn init(&mut self, organs_mutex: &Arc<Mutex<Vec<Organ>>>) {
        let organs = organs_mutex.lock().unwrap();
        println!("DummyBuckets thread: Init copying {:?} organs", organs.len());

        for organ in organs.iter() {
            self.organs.push(organ.clone());
        }
    }

    fn update(&mut self, organs_mutex: &Arc<Mutex<Vec<Organ>>>) {
        bucket_update(&mut self.organs);

        if let Ok(mut display_organs) = organs_mutex.lock() {
            for (organ, moved) in display_organs.iter_mut().zip(self.organs.iter()) {
                organ.set_position(*moved.position());
            }
        }
    }
}


fn bucket_update(organs: &mut Vec<Organ>) {
    let forces = {
        let bounds: (Point2D<i32>, Point2D<i32>) = (
            point2(
                organs.iter().map(|o| o.position().x.floor() as i32).min().unwrap(),
                organs.iter().map(|o| o.position().y.floor() as i32).min().unwrap(),
            ),
            point2(
                organs.iter().map(|o| o.position().x.floor() as i32).max().unwrap(),
                organs.iter().map(|o| o.position().y.floor() as i32).max().unwrap(),
            ),
        );
        let bucket_size = 2;

        let mut buckets: Vec<Vec<Vec<&Organ>>> = vec!();
        let (min, max) = bounds;
        for i in 0..(((max.x - min.x) / bucket_size) + 12) as usize {
            buckets.push(vec![]);
            for _ in 0..(((max.y - min.y) / bucket_size) + 12) {
                buckets[i].push(vec![])
            }
        }

        for organ in organs.iter() {
            let pos = organ.position();
            buckets[
                (pos.x.floor() as i32 / bucket_size - min.x) as usize
                ][
                (pos.y.floor() as i32 / bucket_size - min.y) as usize
                ].push(&organ)
        }
        let mut forces = Vec::with_capacity(organs.len());
        for organ in organs.iter() {
            let mut applied_force = Vector2D::<f32>::zero();
            let pos = organ.position();

            let (x, y) = (
                (pos.x.floor() as i32 / bucket_size - min.x) as usize,
                (pos.y.floor() as i32 / bucket_size - min.y) as usize
            );
            let lel = vec!();
            let lil = vec!();
            for (a, b) in [
                (-1, -1),
                (-1, 0),
                (-1, 1),
                (0, 0),
                (0, -1),
                (0, 1),
                (1, -1),
                (1, 0),
                (1, 1),
            ].iter() {
                for &o in buckets.get((x as i32 + a) as usize)
                    .unwrap_or(&lel)
                    .get((y as i32 + b) as usize)
                    .unwrap_or(&lil) {
                    applied_force.add_assign(
                        organ.interaction_vector(o)
                    );
                }
            }

            forces.push(applied_force.mul(0.021))
        }
        forces
    };
    for (i, aaa) in organs.iter_mut().enumerate() {
        aaa.move_with(forces[i]);
    }
}
