use std::sync::{Arc, Mutex};
use std::ops::*;

use euclid::{point2, Point2D, Vector2D, vec2};

use interaction::thread::InteractionAlgorithm;
use organs::organ::{Organ, OrganKind};
use rayon::prelude::*;
use std::time::{SystemTime, Duration};

struct Bucket {
    grid: Vec<Vec<Organ>>,
    bucket_size: f32,
    size: usize,
    min: Point2D<f32>,
    max: Point2D<f32>,
    range: Vector2D<usize>,
}

impl Bucket {
    fn new(size: usize, bucket_size: f32) -> Self {
        let grid = vec!(Vec::with_capacity(16); size * size);

        Bucket {
            grid,
            bucket_size,
            size,
            min: Point2D::zero(),
            max: Point2D::zero(),
            range: Vector2D::zero(),
        }
    }

    pub fn init(&mut self, min: Point2D<f32>, max: Point2D<f32>) {
        let xrange = max.x - min.x;
        let yrange = max.y - min.y;
        self.min = min;
        self.max = max;
        self.range = Vector2D::new(
            xrange.ceil() as usize,
            yrange.ceil() as usize
        );
    }

    pub fn add(&mut self, organ: Organ) {
        let i = self.index(*organ.position());
        self.grid[i].push(organ);
//        if self.min.x > px { self.min.x = px}
//        if self.min.y > py { self.min.y = py}
//        if self.max.x < px { self.max.x = px}
//        if self.max.y < py { self.max.y = py}
    }

    pub fn update_cell(&mut self, organ: &Organ, force: Vector2D<f32>, velocity: Vector2D<f32>) {
        let mut o = {
            let pos = organ.position();
            let i = self.index(*pos);
            let target = &mut self.grid[i];
            let i = target.iter().position(|o| {
                let opos = o.position();
                (opos.y - pos.y).abs() < std::f32::EPSILON &&
                (opos.y - pos.y).abs() < std::f32::EPSILON
            }).unwrap();
            target.remove(i)
        };
        o.set_velocity(velocity);
        o.move_with(force);
        o.set_range(organ.range());
        self.add(o);
    }

    const AROUND: [(i32, i32); 9] = [
        (-1, -1), (-1, 0), (-1, 1),
        (0, -1), (0, 0), (0, 1),
        (1, -1), (1, 0), (1, 1)
    ];

    pub fn get_close_elements(&self, pos: Point2D<f32>) -> Vec<&Organ> {
        let (x, y) = (self.x(pos.x), self.y(pos.y));
        Bucket::AROUND.iter().map(|(a, b)| {
            let (i, j) = (
                (x as i32 + *b) as usize,
                (y as i32 + *a) as usize
            );
            self.grid[i + j * self.size].iter().map(|o| o)
        }).flatten().filter(|&o| *o.position() != pos).collect::<Vec<_>>()
    }

    fn index(&self, pos: Point2D<f32>) -> usize {
        let x = self.x(pos.x);
        let y = self.y(pos.y);
        x + y * self.size
    }

    fn x(&self, x: f32) -> usize {
        let centered = (self.size / 2 - self.range.x / 2) as i32;
        let pos = (x.floor() / self.bucket_size) as i32;
        (centered + pos) as usize
    }

    fn y(&self, y: f32) -> usize {
        let centered = (self.size / 2 - self.range.y / 2) as i32;
        let pos = (y.floor() / self.bucket_size) as i32;
        (centered + pos) as usize
    }

}

pub struct BucketsInteraction {
    organs: Vec<Organ>,
    bucket: Bucket,
    start_time: SystemTime,
}

impl BucketsInteraction {
    pub fn new() -> Self {
        BucketsInteraction {
            organs: vec!(),
            bucket: Bucket::new(256, 3.0),
            start_time: SystemTime::now(),
        }
    }
}

impl InteractionAlgorithm for BucketsInteraction {
    fn init(&mut self, organs_mutex: &Arc<Mutex<Vec<Organ>>>) {
        let organs = organs_mutex.lock().unwrap();
        println!("Buckets thread: Init copying {:?} organs", organs.len());

        for organ in organs.iter() {
            self.organs.push(organ.clone());
        }

        let xmap = organs.iter().map(|o| o.position().x.floor()).collect::<Vec<_>>();
        let ymap = organs.iter().map(|o| o.position().y.floor()).collect::<Vec<_>>();
        let grid_min: Point2D<f32> = point2(
            *xmap.iter().min_by(|&a, &b| a.partial_cmp(b).unwrap()).unwrap(),
            *ymap.iter().min_by(|&a, &b| a.partial_cmp(b).unwrap()).unwrap()
        );
        let grid_max: Point2D<f32> = point2(
            *xmap.iter().max_by(|&a, &b| a.partial_cmp(b).unwrap()).unwrap(),
            *ymap.iter().max_by(|&a, &b| a.partial_cmp(b).unwrap()).unwrap()
        );

        self.bucket.init(grid_min, grid_max);
        for organ in organs.iter() {
            self.bucket.add(organ.clone())
        }
    }

    fn update(&mut self, organs_mutex: &Arc<Mutex<Vec<Organ>>>) {
        let forces = compute_forces(&self.organs, &self.bucket);
        let delta_time = SystemTime::now().duration_since(self.start_time).unwrap();
        bucket_update(
            forces,
            &mut self.organs,
            &mut self.bucket,
            delta_time
        );

        if let Ok(mut display_organs) = organs_mutex.lock() {
            for (organ, moved) in display_organs.iter_mut().zip(self.organs.iter()) {
                organ.set_position(*moved.position());
                organ.set_velocity(*moved.velocity());
                organ.set_range(moved.range());
            }
        }
    }
}

fn compute_forces(organs: &[Organ], bucket: &Bucket) -> Vec<Vector2D<f32>> {
    organs.par_iter().map( |organ| {
        let close_organs = bucket.get_close_elements(*organ.position());
        organ.interaction_vector_with(close_organs)
    }).collect()
}


fn bucket_update(forces: Vec<Vector2D<f32>>,
                 organs: &mut Vec<Organ>,
                 bucket: &mut Bucket,
                 delta_time: Duration) {
    let dt = 0.08;
    for (organ, &force) in organs
        .iter_mut()
    .zip(forces.iter()) {
        if organ.kind() == OrganKind::Muscle {
            organ.set_range(1. + (delta_time.as_millis() as f32 / 800.).cos() / 2.);
        }
        let v = *organ.velocity();
        // drag forces
        let v_abs = v.abs();
        let drag = vec2(v.x * v_abs.x, v.y * v_abs.y).mul(0.5 * 0.5);
        // acceleration as sum of forces
        let acceleration = force.mul(0.5).add(drag);
        let velocity =  v.add(acceleration).mul(dt * 0.5);
        let translation = velocity.mul(dt).add(acceleration.mul(dt * dt * 0.5));
        bucket.update_cell(organ, translation, velocity);
        organ.move_with(translation);
        organ.set_velocity(velocity);
    }
}