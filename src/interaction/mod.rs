pub mod brute_force;
pub mod dummy_buckets;
pub mod quadtree;
pub mod thread;
pub mod buckets;

mod quads;