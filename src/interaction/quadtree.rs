use std::cell::RefCell;
use std::rc::Rc;
use std::ops::*;
use std::sync::{Arc, Mutex};

use interaction::quads::quads::QuadTree;
use organs::organ::Organ;
use euclid::{Vector2D, rect};
use interaction::thread::InteractionAlgorithm;

pub struct QuadTreeInteraction {
    tree: QuadTree,
    organs_list: Vec<Rc<RefCell<Organ>>>
}

impl QuadTreeInteraction {
    pub fn new() -> Self {
        let tree = QuadTree::new(
            rect(-200., -200., 400., 400.)
        );
        QuadTreeInteraction {
            tree,
            organs_list: vec!(),
        }
    }
}

impl InteractionAlgorithm for QuadTreeInteraction {
    fn init(&mut self, organs_mutex: &Arc<Mutex<Vec<Organ>>>) {
        println!("QuadTree Thread: Starting initialization");

        let organs = {
            let organs = organs_mutex.lock().unwrap();
            println!("QuadTree Thread: Copying in local thead {:?} organs", organs.len());
            organs.iter()
                .map(|o| Rc::new(RefCell::new(o.clone())))
                .collect::<Vec<_>>()
        };

        println!("QuadTree Thread: Starting QuadTree filling with cloned organs");
        for rc in organs.iter() {
            self.tree.add(rc.clone());
        }

        self.organs_list = organs;
        println!("QuadTree Thread: Initialisation Ok");
    }

    fn update(&mut self, organs_mutex: &Arc<Mutex<Vec<Organ>>>) {
        // Compute interaction on local copy of organs
        _quadtree_interaction(&mut self.tree, &mut self.organs_list);

        // Update display data
        if let Ok(mut display_organs) = organs_mutex.lock() {
            for (organ, moved) in display_organs.iter_mut().zip(self.organs_list.iter()) {
                organ.set_position(*moved.borrow().position());
            }
        }
    }
}

fn _quadtree_interaction(quadtree: &mut QuadTree, organs: &mut Vec<Rc<RefCell<Organ>>>) {
    let mut force_by_organ = Vec::with_capacity(organs.len());
    for organ in organs.iter() {
        let mut forces = Vector2D::zero();
        let borrowed_organ = organ.borrow();
        for interacting in quadtree.get_all_near(*borrowed_organ.position(), 3.5).iter() {
            let interacting = interacting.borrow();
            forces.add_assign(borrowed_organ.interaction_vector(&interacting));
        }
        force_by_organ.push(forces.mul(0.02))
    }
    for (organ, &force) in organs.iter_mut().zip(force_by_organ.iter()) {
        let old_position = *organ.borrow().position();
        let old_elem = quadtree.pop_elem(old_position).unwrap();
        old_elem.borrow_mut().move_with(force);
        quadtree.add(old_elem);
    }
}
