use std::sync::{Arc, Mutex};
use std::{thread, time};
use std::thread::JoinHandle;

//use gui::gl_ui::with_frame_rate;
use organs::organ::Organ;

use interaction::quadtree::QuadTreeInteraction;
use interaction::brute_force::BruteForceInteraction;
use interaction::dummy_buckets::DummyBucketsInteraction;
use interaction::buckets::BucketsInteraction;

#[derive(Clone)]
pub struct Conf {
    algo: Algorithm,
    max_framerate: u32,
}

#[derive(Clone, Debug)]
pub enum Algorithm {
    BruteForce,
    DummyBuckets,
    Buckets,
    QuadTree,
}

pub struct InteractionThread {
    conf: Conf,
    organs_mutex: Arc<Mutex<Vec<Organ>>>
}

pub trait InteractionAlgorithm {
    fn init(&mut self, organs_mutex: &Arc<Mutex<Vec<Organ>>>);
    fn update(&mut self, organs_mutex: &Arc<Mutex<Vec<Organ>>>);
}

impl InteractionThread {
    pub fn new(organs_mutex: Arc<Mutex<Vec<Organ>>>, algo: Algorithm) -> Self {
        InteractionThread {
            organs_mutex,
            conf: Conf {
                algo,
                max_framerate: 100_000,
            }
        }
    }

    pub fn start(&mut self) -> JoinHandle<()> {
        let organs_mutex = self.organs_mutex.clone();
        let conf = self.conf.clone();
        thread::spawn(move || {
            let mut num_frames = 0;
            let mut start_time = time::Instant::now();

            let mut algo: Box<dyn InteractionAlgorithm> = match conf.algo {
                Algorithm::BruteForce => Box::new(BruteForceInteraction::new()),
                Algorithm::DummyBuckets => Box::new(DummyBucketsInteraction::new()),
                Algorithm::QuadTree => Box::new(QuadTreeInteraction::new()),
                Algorithm::Buckets => Box::new(BucketsInteraction::new())
            };
            algo.init(&organs_mutex);

            loop {
//                with_frame_rate(conf.max_framerate, || {
                    algo.update(&organs_mutex);

                    num_frames += 1;
                    if time::Instant::now().duration_since(start_time) > time::Duration::from_secs(1) {
                        start_time = time::Instant::now();
                        println!("Processing FPS: {:?}", num_frames);
                        num_frames = 0;
                    }
//                });
            }
        })
    }
}

