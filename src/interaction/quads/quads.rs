use euclid::{Point2D, Rect, rect};
use std::cell::RefCell;
use std::rc::Rc;
use organs::organ::Organ;

const MAX_BUCKET_SIZE: usize = 32;

#[derive(Debug)]
pub struct QuadTree {
    bounds: Rect<f32>,
    sons: Vec<Self>,
    data: Vec<Rc<RefCell<Organ>>>,
}

impl QuadTree {
    pub fn new(bounds: Rect<f32>) -> Self {
        QuadTree {
            bounds,
            sons: Vec::with_capacity(5),
            data: Vec::with_capacity(MAX_BUCKET_SIZE),
        }
    }

    fn create_sons(&mut self) {
        let new_w = self.bounds.size.width / 2.0;
        for (x, y) in [(0., 0.), (1., 0.), (1., 1.), (0., 1.)].iter() {
            let new_bounds = rect(
                self.bounds.origin.x + x * new_w,
                self.bounds.origin.y + y * new_w,
                new_w,
                new_w,
            );
            self.sons.push(Self::new(new_bounds));
        }
    }

    fn add_to_sons(&mut self, data: Rc<RefCell<Organ>>) {
        for son in self.sons.iter_mut() {
            son.add(data.clone())
        }
    }

    fn split_data_to_new_sons(&mut self) {
        self.create_sons();
        let data_ref = self.data.clone();
        self.data.clear();
        for d in data_ref.iter() {
            self.add_to_sons(d.clone());
        }
    }

    pub fn add(&mut self, data: Rc<RefCell<Organ>>) {
        if !self.bounds.contains(data.borrow().position()) {
            return
        }
        if self.is_leaf() && !self.is_full() {
            self.data.push(data);
        } else if self.is_leaf() && self.is_full() {
            self.split_data_to_new_sons();
            self.add_to_sons(data);
        } else {
            self.add_to_sons(data);
        }
    }

    pub fn pop_elem(&mut self, from: Point2D<f32>) -> Option<Rc<RefCell<Organ>>> {
        if !self.bounds.contains(&from) {
            return None;
        }
        if self.is_leaf() {
            if let Some(i) = self.data.iter().position(|d| {
                *d.borrow().position() == from
            }) {
                return Some(self.data.remove(i))
            } else {
                return None
            }
        } else {
            for son in self.sons.iter_mut() {
                if let Some(elem) = son.pop_elem(from) {
                    return Some(elem)
                }
            }
        }
        None
    }

    pub fn get_all_near(&self, origin: Point2D<f32>, range: f32) -> Vec<Rc<RefCell<Organ>>> {
        let mut result = vec!();

        if rect_circle_intersect(self.bounds, origin, range) {
            if self.is_leaf() {
                result.append(&mut self.data.to_vec());
            } else {
                for son in self.sons.iter() {
                    result.append(&mut son.get_all_near(origin, range));
                }
            }
        }
        result
    }

    fn is_full(&self) -> bool {
        self.data.len() > MAX_BUCKET_SIZE
    }

    fn is_leaf(&self) -> bool {
        self.sons.is_empty()
    }
}

fn rect_circle_intersect(rect: Rect<f32>, origin: Point2D<f32>, range: f32) -> bool {
    let rect_max_x = rect.origin.x.max(origin.x.min(rect.origin.x + rect.size.width));
    let rect_max_y = rect.origin.y.max(origin.y.min(rect.origin.y + rect.size.height));
    let deltax = origin.x - rect_max_x;
    let deltay = origin.y - rect_max_y;
    deltax.powi(2) + deltay.powi(2) < range.powi(2)
}