extern crate core;
extern crate clap;
extern crate glutin;
extern crate euclid;
extern crate rand;
extern crate rayon;
extern crate image;

extern crate gl;
extern crate octopus;

pub mod entity_from_string;
mod gls;
mod gui;
mod interaction;
mod configuration;
pub mod organs;

use std::sync::{Arc, Mutex};

use entity_from_string::EntityFromString;
use gui::gl_ui::GlUi;
use interaction::thread::InteractionThread;
use configuration::Configuration;

fn main() {
    let conf = Configuration::load();

    println!("{:#?}", conf);

    let organs = EntityFromString::new(conf.input.clone()).generate().unwrap();

    let organs_mutex = Arc::new(Mutex::new(organs));
    let mut thead_handles = vec![];

    let mut interaction_thread = InteractionThread::new(
        organs_mutex.clone(),
        conf.algo_conf.clone()
    );
    let mut ui = GlUi::new(conf);

    thead_handles.push(interaction_thread.start());
    ui.main_loop(organs_mutex);
}
