use euclid::{point2, Point2D, Vector2D};
use std::ops::{Sub, Mul, AddAssign, Div, Add};

#[derive(Debug, PartialEq, Clone)]
pub struct Organ {
    position: Point2D<f32>,
    velocity: Vector2D<f32>,
    kind: OrganKind,
    range: f32,
}

impl Organ {
    pub fn new(x: f32, y: f32, kind: OrganKind, range: f32) -> Organ {
        let position = point2(x, y);
        Organ {
            position,
            kind,
            velocity: Vector2D::zero(),
            range,
        }
    }

    pub fn position(&self) -> &Point2D<f32> { &self.position }

    pub fn set_position(&mut self, p: Point2D<f32>) {
        self.position = p;
    }

    pub fn range(&self) -> f32 {
        self.range
    }

    pub fn set_range(&mut self, range: f32) {
        let r = self.range;
        let percents_10 = r / 60.;
        let mut range = range;
        range = if range < 0. { range.min(-0.95) } else { range.max(0.95) };
        if (r - range).abs() > percents_10 {
            if r > 0. {
                range = r + percents_10
            } else {
                range = r - percents_10
            }
        }
        self.range = range
    }

    pub fn kind(&self) -> OrganKind {
        self.kind
    }

    pub fn velocity(&self) -> &Vector2D<f32> {
        &self.velocity
    }

    pub fn set_velocity(&mut self, velocity: Vector2D<f32>) {
        self.velocity = velocity
    }

    pub fn distance(&self, other: &Self) -> f32 {
        let (p1, p2) = (&self.position, &other.position);
        let summed_squared = (p1.x - p2.x).powi(2) + (p1.y - p2.y).powi(2);
        summed_squared.sqrt()
    }

    pub fn interaction_vector_with(&self, others: Vec<&Organ>) -> Vector2D<f32> {
        let num = others.len() as f32;
        others.iter().map(|&o| {
            self.interaction_vector(o).div(num)
        }).fold(Vector2D::<f32>::zero(),
                |acc, f| {
                    acc.add(f)
                }
        )
    }

    pub fn interaction_vector(&self, other: &Self) -> Vector2D<f32> {
        let range = (self.range + other.range) / 2.;
        let (apply_effect, cell_effect): (fn(f32, f32) -> f32, f32) = match (&self.kind, &other.kind) {
            (OrganKind::BoneToTendon, OrganKind::Bone)      => (Organ::bones_attraction, range),
            (OrganKind::Bone, OrganKind::BoneToTendon)      => (Organ::bones_attraction, range),
            (OrganKind::BoneToTendon, OrganKind::Tendon)    => (Organ::bones_attraction, range),
            (OrganKind::Tendon, OrganKind::BoneToTendon)    => (Organ::bones_attraction, range),
            // (OrganKind::BoneToTendon, OrganKind::BoneToTendon)    => (Organ::tendon_attraction, range),
//            (OrganKind::BoneToTendon, OrganKind::BoneToTendon)      => (minimal_attraction, range),

            (OrganKind::TendonToMuscle, OrganKind::Muscle)  => (Organ::bones_attraction, range),
            (OrganKind::Muscle, OrganKind::TendonToMuscle)  => (Organ::bones_attraction, range),
            (OrganKind::TendonToMuscle, OrganKind::Tendon)  => (Organ::bones_attraction, range),
            (OrganKind::Tendon, OrganKind::TendonToMuscle)  => (Organ::bones_attraction, range),
            // (OrganKind::TendonToMuscle, OrganKind::TendonToMuscle)  => (Organ::tendon_attraction, range),

            (OrganKind::Bone, OrganKind::Bone)      => (Organ::bones_attraction, range),
            (OrganKind::Tendon, OrganKind::Tendon)  => (Organ::tendon_attraction, range),
            (OrganKind::Muscle, OrganKind::Muscle)  => (Organ::muscle_attraction, range),
//            (_, OrganKind::Muscle)  => (Organ::tendon_attraction, range),
//            (OrganKind::Muscle, _)  => (Organ::tendon_attraction, range),

            (_, _) => (Organ::default_repulsion, range)
        };

        let distance = self.distance(other);
        let effect_vector = self.position.sub(other.position).normalize();
        let attraction = apply_effect(distance, cell_effect);
        effect_vector.mul(attraction)
    }

    fn muscle_attraction(x: f32, range: f32) -> f32 {
        -buckingham_derivative(x + 1. - range, 0.3, 5.5, 0.85)
    }

    fn bones_attraction(x: f32, range: f32) -> f32 {
        -buckingham_derivative(x + 1. - range, 4.2, 10.0, 0.75)
    }

    fn tendon_attraction(x: f32, range: f32) -> f32 {
        -buckingham_derivative(x + 1. - range, 0.5, 1.0, 0.7)
    }

    fn default_repulsion(x: f32, range: f32) -> f32 {
        -buckingham_derivative(x + 1. - range, 1.0, 0.01, 1.2)
    }

    pub fn move_with(&mut self, vec: Vector2D<f32>) {
        self.position.add_assign(vec);
    }
}

fn buckingham_derivative(x: f32, a: f32, e: f32, r: f32) -> f32 {
    // not derived
    // (6. / (a - 6.)) * (a * (1. - x / r)).exp() - (a / (a - 6.)) * (r / x).powi(6)
    let pauli_term_derivative = (6. / (a - 6.)) * (a * (1. - x / r)).exp() * (-a / r);
    let wdw_term_derivative = -(a / (a - 6.)) * r.powi(6) * 6. * (1. / x).powi(5) * (-1. / x.powi(2));
    e * (pauli_term_derivative + wdw_term_derivative)
}

#[derive(Debug, PartialEq, Clone, Copy)]
#[repr(i32)]
pub enum OrganKind {
    Bone = 0,
    BoneToTendon = 1,
    Tendon = 2,
    TendonToMuscle = 3,
    Muscle = 4,
}

pub struct OrganBuilder {
    x: f32,
    y: f32
}

impl OrganBuilder {
    pub fn new(x: f32, y: f32) -> Self {
        OrganBuilder {x: x * 0.9, y:  y * 0.9}
    }

    pub fn muscle(&self) -> Organ {
        Organ::new(self.x, self.y, OrganKind::Muscle, 1.0)
    }

    pub fn bone(&self) -> Organ {
        Organ::new(self.x, self.y, OrganKind::Bone, 1.0)
    }

    pub fn tendon(&self) -> Organ {
        Organ::new(self.x, self.y, OrganKind::Tendon, 1.0)
    }

    pub fn tendon_to_muscle(&self) -> Organ {
        Organ::new(self.x, self.y, OrganKind::TendonToMuscle, 1.0)
    }

    pub fn bone_to_tendon(&self) -> Organ {
        Organ::new(self.x, self.y, OrganKind::BoneToTendon, 1.0)
    }

}
