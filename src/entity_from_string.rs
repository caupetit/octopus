use organs::organ::{Organ, OrganBuilder};
use std::fmt;

pub const STRING_TO_WORLD_RATIO: f32 = 1.;

#[derive(Debug, PartialEq)]
pub enum SetupError {
    InvalidInput(String),
}

impl fmt::Display for SetupError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let display_str = match *self {
            SetupError::InvalidInput(ref c) => format!("Invalid input: {}", c),
        };
        write!(f, "{}", display_str)
    }
}

pub struct EntityFromString {
    input: Vec<String>,
}

impl EntityFromString {
    pub fn new(input: Vec<String>) -> Self {
        EntityFromString { input }
    }

    pub fn generate(&self) -> Result<Vec<Organ>, SetupError> {
        let mut organs = Vec::<Organ>::with_capacity(1024);
        for (y, line) in self.input.iter().rev().cloned().enumerate() {
            for (x, &c) in line.as_bytes().iter().enumerate() {
                let with_ratio = |a| -> f32 { a as f32 * STRING_TO_WORLD_RATIO};
                let organ = OrganBuilder::new(with_ratio(x), with_ratio(y));
                if let Some(organ) = char_to_organs(c as char, organ)? {
                    organs.push(organ);
                }
            }
        }

        Ok(organs)
    }
}

fn char_to_organs(c: char, organs: OrganBuilder) -> Result<Option<Organ>, SetupError> {
    match c {
        'o' => Ok(Some(organs.muscle())),
        '&' => Ok(Some(organs.bone_to_tendon())),
        'x' => Ok(Some(organs.bone())),
        '|' => Ok(Some(organs.tendon_to_muscle())),
        't' => Ok(Some(organs.tendon())),
        '-' => Ok(None),
        invalid => Err(SetupError::InvalidInput(format!("{}", invalid))),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn invalid_char_returns_an_error() {
        let input = ["invalid"].iter().map(|&x| String::from(x)).collect();
        let e = EntityFromString::new(input).generate();

        assert_eq!(e, Err(SetupError::InvalidInput("i".into())))
    }
}
