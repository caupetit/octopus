use glutin::WindowedContext;
use euclid::Point2D;

use entity_from_string::STRING_TO_WORLD_RATIO;
use gui::gl_ui::DisplayEnv;

pub struct UiEnv {
    pub windowed_context: WindowedContext,
    pub display_env: DisplayEnv,
    pub fullscreen: bool,
    pub running: bool,
    pub pressed: bool,
    pub cursor_pos: (f64, f64),
    pub last_cursor_pos: (f64, f64),
}


impl UiEnv {
    pub fn new(windowed_context: WindowedContext, center: Point2D<usize>) -> Self {
        let display_env = DisplayEnv {
            screen_size: [0.0, 0.0],
            organs_num: 0,
            center: [center.x as f32 / 2.0 * STRING_TO_WORLD_RATIO, center.y as f32 / 2.0 * STRING_TO_WORLD_RATIO],
            zoom: 0.8,
        };

        UiEnv {
            windowed_context,
            display_env,
            fullscreen: false,
            running: true,
            pressed: false,
            cursor_pos: (0.0, 0.0),
            last_cursor_pos: (0.0, 0.0),
        }
    }
    
    pub fn update(&mut self) {
        self.update_screen_size();
        self.update_center_position();
        self.last_cursor_pos = self.cursor_pos;
    }

    fn update_screen_size(&mut self) {
        let dpi_factor = self.windowed_context.get_hidpi_factor();
        let size = self.windowed_context.get_inner_size().unwrap();
        let size= size.to_physical(dpi_factor);
        self.display_env.screen_size = [size.width as f32, size.height as f32];
    }

    fn update_center_position(&mut self) {
        if self.pressed {
            let (lastx, lasty) = self.last_cursor_pos;
            let (curx, cury) = self.cursor_pos;
            self.display_env.center[0] += (lastx - curx)  as f32 * self.display_env.zoom;
            self.display_env.center[1] -= (lasty - cury)  as f32 * self.display_env.zoom;
        }
    }
}