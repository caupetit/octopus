use std::{time, thread};
use std::ops::*;

use glutin::{
    EventsLoop, GlRequest, ContextTrait, Api, GlProfile, Event,
};
use glutin::dpi::LogicalSize;

use configuration::Configuration;
use gls::uniform_block::UniformBlock;
use gui::ui_env::UiEnv;
use std::sync::{Arc, Mutex};
use gui::organ_renderer::OrganRenderer;
use gls::buffer::GlBufferObject;
use organs::organ::Organ;


pub struct DisplayEnv {
    pub screen_size: [f32; 2],
    pub center: [f32; 2],
    pub zoom: f32,
    #[allow(dead_code)]
    pub organs_num: i32, // Used in fragment shader
}


pub trait Render {
    fn init(&mut self) -> ();
    fn render(&self) -> ();
}


pub struct GlUi {
    el: EventsLoop,
    ue: UiEnv,
    renderers: Vec<Box<dyn Render>>,
    display_env_ubo: UniformBlock<DisplayEnv>,
    organs_buffer: GlBufferObject,
}


impl GlUi {
    pub fn new(conf: Configuration) -> Self {
        let el = glutin::EventsLoop::new();
        let wb = glutin::WindowBuilder::new()
            .with_dimensions(LogicalSize::new(900.0, 600.0))
            .with_title("Octo");
        let windowed_context = glutin::ContextBuilder::new()
            .with_vsync(false)
            .with_gl(GlRequest::Specific(Api::OpenGl, (4, 5)))
            .with_gl_profile(GlProfile::Core)
            .build_windowed(wb, &el)
            .unwrap();

        unsafe {
            windowed_context.make_current().unwrap();
        }
        gl::load_with(|symbol| windowed_context.get_proc_address(symbol) as *const _);
        unsafe {
            gl::ClearColor(0.3, 0.2, 0.4, 1.0);
        }

        let env_uniform_block = UniformBlock::<DisplayEnv>::new(
            "Env",
            0, 1,
            gl::DYNAMIC_DRAW
        );

        let organ_size = std::mem::size_of::<Organ>();
        let organs_buffer =  GlBufferObject::new(
            gl::ARRAY_BUFFER,
            conf.max_organs * organ_size,
            std::ptr::null(),
            gl::DYNAMIC_DRAW
        );

        let mut organ_renderer = OrganRenderer::new(
            &env_uniform_block,
            conf.max_organs,
        );

        organ_renderer.vao.bind_updatable_buffer(&organs_buffer, 2, organ_size, gl::FLOAT,0);
        // we do not send the velocity to the shaders
        organ_renderer.vao.bind_updatable_ibuffer(&organs_buffer, 1, organ_size, gl::INT, 4);
        organ_renderer.vao.bind_updatable_buffer(&organs_buffer, 1, organ_size, gl::FLOAT, 5);

        let renderers: Vec<Box<dyn Render>> = vec!(Box::new(organ_renderer));

        GlUi {
            el,
            ue: UiEnv::new(windowed_context, conf.entity_size),
            organs_buffer,
            display_env_ubo: env_uniform_block,
            renderers
        }
    }

    fn init_render(&self) {
        let size = self.ue.windowed_context.get_inner_size().unwrap();
        unsafe {
            gl::Viewport(0, 0, size.width as i32, size.height as i32);
            gl::Clear(gl::COLOR_BUFFER_BIT);
            gl::Enable(gl::BLEND);
            gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
        }
    }

    fn poll_events(&mut self) {
        let ue = &mut self.ue;
        self.el.poll_events(|event| {
            consume_event(event, ue);
        });
    }

    pub fn main_loop(&mut self, organs_mutex:  Arc<Mutex<Vec<Organ>>>) {
        {
            let organs = organs_mutex.lock().unwrap();
            self.ue.display_env.organs_num = organs.len() as i32;
        }
        for renderer in &mut self.renderers {
            renderer.init();
        }
        let mut running = self.ue.running;
        let mut num_frames = 0;
        let mut start_time = time::Instant::now();
        while running {
            with_frame_rate(60, || {
                self.poll_events();

                self.ue.update();
                self.display_env_ubo.load_data_from(&self.ue.display_env as *const _);
                {
                    let organs = organs_mutex.lock().unwrap();
                    self.organs_buffer.bind();
                    unsafe {
                        gl::BufferData(
                            gl::ARRAY_BUFFER,
                            (organs.len() * std::mem::size_of::<Organ>()) as gl::types::GLsizeiptr,
                            organs.as_ptr() as *const _,
                            gl::DYNAMIC_DRAW
                        );
                    }
                    self.organs_buffer.unbind();
                }

                self.init_render();
                for renderer in &self.renderers {
                    renderer.render();
                }
                self.ue.windowed_context.swap_buffers().unwrap();
            });

            num_frames += 1;

            if time::Instant::now().duration_since(start_time) > time::Duration::from_secs(1) {
                start_time = time::Instant::now();
                println!("FPS: {:?}", num_frames);
                num_frames = 0;
            }
            running = self.ue.running;
        }
    }
}

pub fn with_frame_rate<F: FnMut()>(
    frame_rate: u32,
    mut process_frame: F) {
    let frame_time = time::Duration::from_secs(1).div(frame_rate);
    let frame_start_time = time::Instant::now();

    process_frame();

    let frame_duration = time::Instant::now().duration_since(frame_start_time);
    if let Some(sleep_time) = frame_time.checked_sub(frame_duration) {
        thread::sleep(sleep_time);
    }
}

fn consume_event(event: Event, ue: &mut UiEnv) {
    let dpi_factor = ue.windowed_context.get_hidpi_factor();
    if let glutin::Event::WindowEvent { event, .. } = event {
        match event {
            glutin::WindowEvent::CloseRequested => ue.running = false,
            glutin::WindowEvent::Resized(logical_size) => {
                ue.windowed_context.resize(logical_size.to_physical(dpi_factor));
            },
            glutin::WindowEvent::MouseInput { state, button: glutin::MouseButton::Left, .. } => {
                ue.pressed = match state {
                    glutin::ElementState::Pressed => { true },
                    glutin::ElementState::Released => { false }
                }
            },
            glutin::WindowEvent::CursorMoved { position, .. } => {
                ue.cursor_pos = (position.x, position.y);
            }
            glutin::WindowEvent::MouseWheel { delta, .. } => {
                let diff = match delta {
                    glutin::MouseScrollDelta::PixelDelta(pos) => { pos.y as f32 },
                    glutin::MouseScrollDelta::LineDelta(_, y) => { y }
                };
                ue.display_env.zoom += diff * 0.01;
                if ue.display_env.zoom < 0.02 { ue.display_env.zoom = 0.02 }
            },
            glutin::WindowEvent::KeyboardInput { input, .. } => {
                match input.virtual_keycode {
                    Some(glutin::VirtualKeyCode::Escape) => ue.running = false,
                    Some(glutin::VirtualKeyCode::F)
                    if input.state == glutin::ElementState::Pressed =>
                        {
                            let monitor = if ue.fullscreen {
                                None
                            } else {
                                Some(ue.windowed_context.get_current_monitor())
                            };
                            ue.windowed_context.set_fullscreen(monitor);
                            ue.fullscreen = !ue.fullscreen;
                        }
                    _ => (),
                }
            }
            _ => (),
        }
    }
}
