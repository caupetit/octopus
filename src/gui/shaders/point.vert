#version 330 core

layout (location = 0) in vec3 Position;
layout (location = 1) in vec2 aTexCoord;
layout (location = 2) in vec2 o_pos;
layout (location = 3) in int o_kind;
layout (location = 4) in float o_range;

layout (std140) uniform Env
{
    vec2 ScreenSize;
    vec2 center;
    float zoom;
    int organs_num;
};

//flat out vec2 position;
flat out int kind;
//flat out float range;
out vec2 TexCoord;

void main()
{
//    position = o_pos;
    kind = o_kind;
//    range = o_range;

//    gl_Position = vec4(Position, 1.0);
    TexCoord = aTexCoord;
    float z  = zoom * 2.;

    float square_pixels = ScreenSize.x * 3.2 / 100.;
    float square_size = square_pixels * z * o_range;
    vec2 organ_pos = (center - o_pos) / 20 * z;
    vec2 screen_pos = square_size * (Position.xy / ScreenSize) - organ_pos;
    gl_Position = vec4(screen_pos, 0.0, 1.0);
}
