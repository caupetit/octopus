#version 330

out vec4 FragColor;

//layout (std140) uniform Env
//{
//    vec2 ScreenSize;
//    vec2 center;
//    float zoom;
//    int organs_num;
//};

//flat in vec2 position;
flat in int kind;
//flat in float range;
in vec2 TexCoord;

uniform sampler2D ourTexture;

void main()
{
//    color = vec4(float(kind) / 4.0, float(kind) / 32.0, 0, 1);

    vec4 color = vec4(0.6, 0.6, 0.6, 1);
    if ( kind != 2) {
        color = vec4(float(kind) / 4.0, float(kind) / 32.0, 0, 0.9);
    }

    //    color = texture(ourTexture, TexCoord) + vec4(0.5, 0., 0., 0.5);
    FragColor = texture(ourTexture, TexCoord) * color;
//    float world_ratio = ScreenSize.x / 40.0 * zoom;
//    vec2 screen_center = ScreenSize.xy / 2;
//
//    vec2 screen_pos = screen_center + (position - center) * world_ratio;
//    float distance = distance(gl_FragCoord.xy, screen_pos);
//    float circle_radius = world_ratio / 2 * range;
//    if (distance < circle_radius) {
//        if ( kind != 2) {
//            color = vec4(float(kind) / 4.0, float(kind) / 32.0, 0, 1);
//        } else {
//            color = vec4(0.5, 0.5, 0.5, 1);
//        }
//    }
//    else if (distance > circle_radius && distance < circle_radius + 1) {
//        color = vec4(0.5, 0.5, 1.0, 0.6);
//    }
}
