use gls::array::GlArrayObject;
use ::gls;
use gls::buffer::GlBufferObject;
use gls::uniform_block::UniformBlock;
use gui::gl_ui::{Render, DisplayEnv};
use image::GenericImageView;
use std::ffi::c_void;

pub struct OrganRenderer  {
    pub vao: GlArrayObject,
    texture: gl::types::GLuint,
    shader_program: gls::program::Program,
    elements_num: usize,
    _img: Vec<u8>,
}

impl OrganRenderer {
    pub fn new(env_ubo: &UniformBlock<DisplayEnv>, elements_num: usize) -> Self {
        use std::ffi::CString;
        let source = &CString::new(include_str!("shaders/point.vert")).unwrap();
        let vert_shader = gls::shader::Shader::from_vert_source(source).unwrap();

        let source = &CString::new(include_str!("shaders/point.frag")).unwrap();
        let frag_shader = gls::shader::Shader::from_frag_source(source).unwrap();

        let shader_program = gls::program::Program::from_shaders(&[vert_shader, frag_shader]).unwrap();

        let vertices: Vec<f32> = vec![
            // square vertices  // corresponding texel coordinates
            1.0,   1.0,  0.0,   1.0, 1.0,   // top right
            1.0,   -1.,  0.0,   1.0, 0.0,   // bottom right
            -1.0,  -1.0, 0.0,   0.0, 0.0,   // bottom left
            -1.0,  1.0,  0.0,   0.0, 1.0,   // top left
        ];

        let indices: Vec<u32> = vec![
            0, 1, 2,
            0, 2, 3
        ];

        let ebo = GlBufferObject::new(
            gl::ELEMENT_ARRAY_BUFFER,
            indices.len() * std::mem::size_of::<u32>(),
            indices.as_ptr() as *const gl::types::GLvoid,
            gl::STATIC_DRAW
        );

        let vbo = GlBufferObject::new(
            gl::ARRAY_BUFFER,
            vertices.len() * std::mem::size_of::<f32>(),
            vertices.as_ptr() as *const gl::types::GLvoid,
            gl::STATIC_DRAW
        );

        let mut vao = GlArrayObject::new(vbo.clone(), ebo);

        let img_data = include_bytes!("textures/circe1.1.png");
        let img = image::load_from_memory(img_data).unwrap();
        let (width, height) = img.dimensions();
        let img_data = img.to_rgba().into_raw();
        let mut texture_id: gl::types::GLuint = 0;
        unsafe {
            gl::GenTextures(1, &mut texture_id);
            gl::BindTexture(gl::TEXTURE_2D, texture_id);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::NEAREST as i32);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32);
            gl::TexImage2D(
                gl::TEXTURE_2D, 0, gl::RGBA as i32,
                width as i32, height as i32,
                0, gl::RGBA, gl::UNSIGNED_BYTE,
                img_data.as_ptr() as *const _,
            );
            vao.bind();
            vbo.bind();
            gl::VertexAttribPointer(
                vao.vbo_num, 2 as i32,
                gl::FLOAT, gl::FALSE,
                5 * std::mem::size_of::<f32>() as i32,
                std::ptr::null::<c_void>().offset(3 * std::mem::size_of::<i32>() as isize)
            );
            gl::EnableVertexAttribArray(vao.vbo_num);
            vao.vbo_num += 1;
        }

        env_ubo.bind_to_program(shader_program.id());
        shader_program.set_used();
        OrganRenderer {
            vao,
            shader_program,
            texture: texture_id,
            elements_num,
            _img: img_data
        }
    }
}


impl Render for OrganRenderer {
    fn init(&mut self) {
        self.shader_program.set_used();
    }

    fn render(&self) {
        unsafe {
            self.vao.bind();
            gl::ActiveTexture(gl::TEXTURE0);
            gl::BindTexture(gl::TEXTURE_2D, self.texture);
            gl::DrawElementsInstanced(
                gl::TRIANGLES, // mode
                6,             // number of indices to be rendered
                gl::UNSIGNED_INT,
                std::ptr::null(),
                self.elements_num as gl::types::GLint,
            );
            gl::BindVertexArray(0);
        }
    }
}
