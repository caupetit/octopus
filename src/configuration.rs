use clap::{App, Arg, SubCommand};
use euclid::{Point2D, point2};
use rand::seq::SliceRandom;
use std::path::Path;
use std::fs::File;
use std::io::{BufReader, BufRead};

use interaction::thread::Algorithm;

const CHARS: [char; 9] = ['x', 'x', '&', 'o', 'o', '|', 't', 't', '-'];

#[derive(Debug)]
pub struct Configuration {
    pub input: Vec<String>,
    pub entity_size: Point2D<usize>,
    pub algo_conf: Algorithm,
    pub max_organs: usize,
}

const DEFAULT_WIDTH: &str = "80";
const DEFAULT_HEIGHT: &str = "60";

impl Configuration {
    pub fn load() -> Self {
        let matches = App::new("Octopus")
            .arg(Arg::with_name("algo")
                .default_value("buckets")
                .short("a"))
            .subcommand(SubCommand::with_name("f")
                .about("FILE Load organs from file")
                .arg(Arg::with_name("file")
                    .help("The path to the file to use as organs array")
                    .index(1)
                    .required(true)))
            .subcommand(SubCommand::with_name("r")
                .about("WIDTH HEIGHT Generate random organs in a grid from width and height")
                .arg(Arg::with_name("width")
                    .help("width")
                    .default_value(DEFAULT_WIDTH)
                    .index(1))
                .arg(Arg::with_name("height")
                    .help("height")
                    .default_value(DEFAULT_HEIGHT)
                    .index(2)))
            .get_matches();

        let algo = matches.value_of("algo").unwrap().to_string();
        let algo = match algo.to_lowercase().as_ref() {
            "quadtree" => Algorithm::QuadTree,
            "dummybuckets" => Algorithm::DummyBuckets,
            "bruteforce" => Algorithm::BruteForce,
            "buckets" | _ => Algorithm::Buckets,
        };

        let mut entity_size = point2(
            DEFAULT_WIDTH.parse::<usize>().unwrap(),
            DEFAULT_HEIGHT.parse::<usize>().unwrap(),
        );
        let input = match matches.subcommand() {
            ("f", Some(sub_m)) => {
                let file_path = sub_m.value_of("file").unwrap();
                println!("File path: {:?}", file_path);
                let input = lines_from_file(file_path);
                let height = input.len();
                let width = input.iter().map(|s| s.len()).max().unwrap();
                entity_size = point2(width, height);
                input
            },
            ("r", Some(sub_m)) => {
                let width = sub_m.value_of("width").unwrap().parse::<usize>().unwrap();
                let height = sub_m.value_of("height").unwrap().parse::<usize>().unwrap();
                assert!(width < 128);
                assert!(height < 128);
                entity_size = point2(width, height);
                generate(width, height)
            },
            _ => {
                generate(entity_size.x, entity_size.y)
            }
        };

        Configuration {
            entity_size,
            input,
            max_organs: entity_size.x * entity_size.y,
            algo_conf: algo
        }
    }
}

fn generate(w: usize, h: usize) -> Vec<String> {
    let mut result = vec![String::new(); h];
    for s in &mut result {
        for _ in 0..w {
            s.push(CHARS.choose(&mut rand::thread_rng()).unwrap().clone())
        }
    }
    result
}

fn lines_from_file(filename: impl AsRef<Path>) -> Vec<String> {
    let file = File::open(filename).expect("no such file");
    let buf = BufReader::new(file);
    buf.lines()
        .map(|l| l.expect("Could not parse line"))
        .collect()
}
