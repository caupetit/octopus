use std::ffi::CStr;
use gls::handle_gl_errors;

pub struct Shader {
    id: gl::types::GLuint,
}

impl Shader {
    fn from_source(source: &CStr, kind: gl::types::GLenum) -> Result<Shader, String> {
        let id = unsafe { gl::CreateShader(kind) };

        unsafe {
            gl::ShaderSource(id, 1, &source.as_ptr(), std::ptr::null());
            gl::CompileShader(id);
        }

        handle_gl_errors(
            id,
            gl::COMPILE_STATUS,
            gl::GetShaderiv,
            gl::GetShaderInfoLog,
        )?;

        Ok(Shader { id })
    }

    pub fn from_vert_source(source: &CStr) -> Result<Shader, String> {
        Shader::from_source(source, gl::VERTEX_SHADER)
    }

    pub fn from_frag_source(source: &CStr) -> Result<Shader, String> {
        Shader::from_source(source, gl::FRAGMENT_SHADER)
    }

    pub fn attach(&self, program_id: gl::types::GLuint) {
        unsafe {
            gl::AttachShader(program_id, self.id);
        }
    }

    pub fn detach(&self, program_id: gl::types::GLuint) {
        unsafe {
            gl::AttachShader(program_id, self.id);
        }
    }
}

impl Drop for Shader {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteShader(self.id);
        }
    }
}
