use gls::buffer::GlBufferObject;
use std::os::raw::c_void;

pub struct GlArrayObject {
    pub id: gl::types::GLuint,
    pub vbo_num: gl::types::GLuint,
}


impl GlArrayObject {
    pub fn new(vbo: GlBufferObject, ebo: GlBufferObject) -> Self {
        let mut id: gl::types::GLuint = 0;
        unsafe {
            gl::GenVertexArrays(1, &mut id);
            gl::BindVertexArray(id);
            vbo.bind();
            ebo.bind();
            gl::VertexAttribPointer(
                0,
                3,
                gl::FLOAT,
                gl::FALSE,
                (5 * std::mem::size_of::<f32>()) as gl::types::GLint,
                std::ptr::null(),
            );
            gl::EnableVertexAttribArray(0); // this is "layout (location = 0)" in vertex shader
            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
            gl::BindVertexArray(0);
        }

        GlArrayObject { id, vbo_num: 1 }
    }

    pub fn bind_updatable_buffer(
        &mut self,
        buffer: &GlBufferObject,
        ct: usize,
        size: usize,
        gl_data_type: gl::types::GLenum,
        offset: isize,
    ) {
        unsafe {
            gl::BindVertexArray(self.id);
            buffer.bind();
            gl::EnableVertexAttribArray(self.vbo_num);
            gl::VertexAttribPointer(
                self.vbo_num,
                ct as gl::types::GLint,
                gl_data_type,
                gl::FALSE,
                size as gl::types::GLint,
                std::ptr::null::<c_void>().offset(offset * std::mem::size_of::<f32>() as isize),
            );
            // to be reread on each draw calls
            gl::VertexAttribDivisor(self.vbo_num, 1);
            buffer.unbind();
        }
        self.vbo_num += 1;
    }


    pub fn bind_updatable_ibuffer(
        &mut self,
        buffer: &GlBufferObject,
        ct: usize,
        size: usize,
        gl_data_type: gl::types::GLenum,
        offset: isize,
    ) {
        unsafe {
            gl::BindVertexArray(self.id);
            buffer.bind();
            gl::EnableVertexAttribArray(self.vbo_num);
            gl::VertexAttribIPointer(
                self.vbo_num,
                ct as gl::types::GLint,
                gl_data_type,
                size as gl::types::GLint,
                std::ptr::null::<c_void>().offset(offset * std::mem::size_of::<i32>() as isize),
            );
            // to be reread on each draw calls
            gl::VertexAttribDivisor(self.vbo_num, 1);
            buffer.unbind();
        }
        self.vbo_num += 1;
    }

    pub fn bind(&self) {
        unsafe {
            gl::BindVertexArray(self.id);
        }
    }
}