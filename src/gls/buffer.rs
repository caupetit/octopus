

#[derive(Clone)]
pub struct GlBufferObject {
    id: gl::types::GLuint,
    kind: u32,
}


impl GlBufferObject {
    pub fn new(kind: u32, size: usize, data: *const gl::types::GLvoid, mode: u32) -> Self {
        let mut id: gl::types::GLuint = 0;
        unsafe {
            gl::GenBuffers(1, &mut id);
            gl::BindBuffer(kind, id);
            gl::BufferData(kind, size as gl::types::GLsizeiptr, data, mode);
            gl::BindBuffer(kind, 0);
        }
        GlBufferObject { id, kind }
    }

    pub fn bind(&self) {
        unsafe {
            gl::BindBuffer(self.kind, self.id);
        }
    }

    pub fn unbind(&self) {
        unsafe {
            gl::BindBuffer(self.kind, 0);
        }
    }

    pub fn id(&self) -> gl::types::GLuint {
        self.id
    }
}