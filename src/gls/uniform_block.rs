use gls::buffer::GlBufferObject;
use std::marker::PhantomData;
use std::ffi::CString;

pub struct UniformBlock<T> {
    block_name: CString,
    binding_id: gl::types::GLuint,
    size: usize,
    ubo: GlBufferObject,
    data_type: PhantomData<T>
}

impl<T> UniformBlock<T> {
    pub fn new(
        name: &str,
        binding_id: u32,
        data_number: usize,
        mode: u32,
    ) -> Self {

        let size = std::mem::size_of::<T>() * data_number;
        let ubo = GlBufferObject::new(
            gl::UNIFORM_BUFFER,
            size,
            std::ptr::null(),
            mode
        );

        let block_name = CString::new(name).unwrap();
        unsafe {
            gl::BindBufferBase(gl::UNIFORM_BUFFER, binding_id, ubo.id());
        };

        UniformBlock {
            block_name,
            binding_id,
            size,
            ubo,
            data_type: PhantomData,
        }
    }

    pub fn bind_to_program(&self, program_id: gl::types::GLuint) {
        unsafe {
            let block_id = gl::GetUniformBlockIndex(
                program_id,
                self.block_name.as_ptr() as *const gl::types::GLchar
            );
            gl::UniformBlockBinding(program_id, block_id, self.binding_id);
        }
    }

    pub fn load_data_from(&self, data: *const T) {
        unsafe {
            self.ubo.bind();
            gl::BufferSubData(
                gl::UNIFORM_BUFFER,
                0,
                self.size as gl::types::GLsizeiptr,
                data as *const std::ffi::c_void
            );
            self.ubo.unbind();
            // TODO Not working on windows ???? check on osx/linux
//            let p = gl::MapBuffer(gl::UNIFORM_BUFFER, gl::WRITE_ONLY);
//            std::ptr::copy_nonoverlapping(
//                data as *const T,
//                p as *mut T,
//                self.size
//            );
//            gl::UnmapBuffer(gl::UNIFORM_BUFFER);
        }
    }
}