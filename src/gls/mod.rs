use std::ffi::CString;

pub mod buffer;
pub mod array;
pub mod uniform_block;
pub mod shader;
pub mod program;

type GlGetIvFnPointer = unsafe fn(
    gl::types::GLuint,
    gl::types::GLenum,
    *mut gl::types::GLint
) -> ();

type GlGetInfoLogFnPointer = unsafe fn(
    gl::types::GLuint,
    gl::types::GLint,
    *mut gl::types::GLint,
    *mut gl::types::GLchar,
) -> ();

fn handle_gl_errors(
    id: gl::types::GLuint,
    iv_kind: gl::types::GLenum,
    get_iv_fn: GlGetIvFnPointer,
    get_info_log_fn: GlGetInfoLogFnPointer,
) -> Result<gl::types::GLuint, String> {
    let mut success: gl::types::GLint = 1;

    unsafe { get_iv_fn(id, iv_kind, &mut success) }

    if success == 0 {
        let mut len: gl::types::GLint = 0;
        unsafe {
            get_iv_fn(id, gl::INFO_LOG_LENGTH, &mut len);
        }
        let error = allocated_c_string(len as usize);

        unsafe {
            get_info_log_fn(
                id,
                len,
                std::ptr::null_mut(),
                error.as_ptr() as *mut gl::types::GLchar,
            );
        }
        return Err(error.to_string_lossy().into_owned());
    }
    Ok(id)
}

fn allocated_c_string(len: usize) -> CString {
    unsafe { CString::from_vec_unchecked(vec![0; len + 1]) }
}
