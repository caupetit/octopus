use gls::shader::Shader;
use gls::handle_gl_errors;

pub struct Program {
    id: gl::types::GLuint,
}

impl Program {
    pub fn from_shaders(shaders: &[Shader]) -> Result<Program, String> {
        let program_id = unsafe { gl::CreateProgram() };

        shaders.iter().for_each(|shader| shader.attach(program_id));
        unsafe {
            gl::LinkProgram(program_id);
        }

        handle_gl_errors(
            program_id,
            gl::LINK_STATUS,
            gl::GetProgramiv,
            gl::GetProgramInfoLog,
        )?;

        shaders.iter().for_each(|shader| shader.detach(program_id));
        Ok(Program { id: program_id })
    }

    pub fn id(&self) -> gl::types::GLuint {
        self.id
    }

    pub fn set_used(&self) {
        unsafe {
            gl::UseProgram(self.id);
        }
    }
}

impl Drop for Program {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteProgram(self.id);
        }
    }
}
