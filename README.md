# Octopus

Project initialy made to provide a training environment for ML algorithms

OpenGL display of thousands of cells in constant interactions


## V2

The cells are interacting with each other using some simple molecular dynamics
using Buckingham potential



5 elements
- x   os
- -x- os-tendon
- \-  tendon
- -o- tendon-muscle
- o   muscle

### organs interaction matrix
```
         x   -x-    \-   -o-   o

 x      ><    ><    <>   <>   <>

-x-     ><    <>    ><   <>   <>

 \-     <>    ><    ><   ><   <>

-o-     <>    <>    ><   ><   ><

 o      <>    <>    <>   ><   ><
```

- [X] entity = 40 * 60
- [X] switchable generation => 1 file / 2 random / etc
- [X] compute the physical interactions
- [ ] display the range of the interactions
- [ ] borders of the total shape computed as segments (rounding organs)
- [ ] display it
- [ ] borders contains moving direction vector
- [ ] display it
- [ ] organs move according to the average of the attached segments
- [ ] segments provide resistance to the movements
- [ ] alive while moving and 95?% num_organs are still in range of another element
- [ ] give tension to muscles

![octopus working example](<img/octopus.png>) 

## Wsl setup
rust >= 1.36

OpenGL 4.6 and glsl 330 is required

```bash
# for opengl on wsl with vcxsrv configured as non native opengl (mesa 1.4)
export MESA_GL_VERSION_OVERRIDE=4.6
export MESA_GLSL_VERSION_OVERRIDE=330
export DISPLAY=localhost:0
```

```bash
./target/release/octopus -h
USAGE:
    octopus [OPTIONS] [SUBCOMMAND]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -a <algo>         [default: buckets]

SUBCOMMANDS:
    f       FILE Load organs from file
    help    Prints this message or the help of the given subcommand(s)
    r       WIDTH HEIGHT Generate random organs in a grid from width and height
```

## V1 - first thoughts

### The environment

- [X] empty infinite environment in 2D
- [X] physics laws - pressure ?

### The agent

- [ ] procedurally generated
- [ ] must mimic a nervous system
- [ ] all the nervous system are the AI input for a given state
- [ ] the output of the AI is the response to return to the nervous system
- [ ] every information about the world is coming from the nervous system
- [ ] some kind of 'organs' can provide nervous system input
- [ ] the total number of inputs given by the nervous system is finite


=> an array of 100 chars (value from 0 to 255) represents the total outputs of the nervous system

organs can consume slots of the nervous system, one is mandatory for each to decide of his ability to influence the nervous system

the 'alive' value of an organ must be between 256 * 1/4 and 256 * 3/4
generations create slots randomly

### possible organs.

- [ ] pressure creating organs
- [ ] pressure sensor organs

an organ owns multiple functions:
